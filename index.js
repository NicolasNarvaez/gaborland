
import express from 'express'

const app = express()

app.use(express.static('public'))

const port = 8160

app.listen(port, () => {
  console.log(`app listening in port: ${port}`)
})
