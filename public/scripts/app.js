var canvas_size = 256

if ( WEBGL.isWebGLAvailable() === false ) {

  document.body.appendChild( WEBGL.getWebGLErrorMessage() );

}

var camera, scene, renderer, mesh, material;
var drawStartPos = new THREE.Vector2();

init();
setupCanvasDrawing();
animate();

function init() {

  camera = new THREE.PerspectiveCamera( 50, window.innerWidth / window.innerHeight, 1, 2000 );
  camera.position.z = 500;

  scene = new THREE.Scene();

  material = new THREE.MeshBasicMaterial();

  mesh = new THREE.Mesh( new THREE.BoxBufferGeometry( 200, 200, 200 ), material );
  scene.add( mesh );

  renderer = new THREE.WebGLRenderer( { antialias: true } );
  renderer.setPixelRatio( window.devicePixelRatio );
  renderer.setSize( window.innerWidth, window.innerHeight );
  document.body.appendChild( renderer.domElement );

  window.addEventListener( 'resize', onWindowResize, false );

}

// Sets up the drawing canvas and adds it as the material map

function setupCanvasDrawing() {

  // get canvas and context

  var drawingCanvas = document.getElementById( 'drawing-canvas' );
  var drawingContext = drawingCanvas.getContext( '2d' );

  // draw white background

  drawingContext.fillStyle = '#FFFFFF';
  drawingContext.fillRect( 0, 0, 256, 256 );

  // set canvas as material.map (this could be done to any map, bump, displacement etc.)

  material.map = new THREE.CanvasTexture( drawingCanvas );

  // set the variable to keep track of when to draw

  var paint = false;

  // add canvas event listeners
  drawingCanvas.addEventListener( 'mousedown', function( e ) {

    paint = true;
    drawStartPos.set( e.offsetX, e.offsetY );

  } );

  drawingCanvas.addEventListener( 'mousemove', function( e ) {

    if( paint ) draw( drawingContext, e.offsetX, e.offsetY );

  } );

  drawingCanvas.addEventListener( 'mouseup', function( e ) {

    paint = false;

  } );

  drawingCanvas.addEventListener( 'mouseleave', function( e ) {

    paint = false;

  } );

}

function draw( drawContext, x, y ) {

  drawContext.moveTo( drawStartPos.x, drawStartPos.y );
  drawContext.strokeStyle = '#000000';
  drawContext.lineTo( x, y );
  drawContext.stroke();
  // reset drawing start position to current position.
  drawStartPos.set( x, y );
  // need to flag the map as needing updating.
  material.map.needsUpdate = true;

}

function onWindowResize() {

  camera.aspect = window.innerWidth / window.innerHeight;
  camera.updateProjectionMatrix();

  renderer.setSize( window.innerWidth, window.innerHeight );

}

function animate() {

  requestAnimationFrame( animate );

  // mesh.rotation.x += 0.01;
  // mesh.rotation.y += 0.01;

  renderer.render( scene, camera );

}
